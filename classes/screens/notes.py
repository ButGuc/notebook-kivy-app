from kivy.uix.screenmanager import Screen
from classes.data.database import database


class notes(Screen):

    category_id = None

    def __init__(self, **kw):
        super().__init__(**kw)

        self.ids['lv_notes'].adapter.bind(on_selection_change=self.lv_notes_on_selection_change)

    def open(self):
        self.ids['lv_notes'].adapter.data = []

        db = database()

        notes_data = db.get_notes(self.category_id)
        category_data = db.get_category(self.category_id)

        self.ids['header'].content = "Notes ( {0} )".format(category_data["category_name"])

        if notes_data:
            self.ids['lv_notes'].adapter.data = notes_data

    @staticmethod
    def lv_notes_args_converter(row, obj):
        return {'text': obj['note_title'], 'size_hint_y': None, 'height': 30}

    def lv_notes_on_selection_change(self, obj):
        if obj.selection:
            index = obj.selection[0].index
            selected = obj.data[index]

            self.parent.get_screen('note').task = 'edit'
            self.parent.get_screen('note').note_id = selected['id']
            self.parent.get_screen('note').open()
            self.parent.current = 'note'
