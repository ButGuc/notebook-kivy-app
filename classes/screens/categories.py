from kivy.uix.screenmanager import Screen
from classes.data.database import database


class categories(Screen):

    def __init__(self, **kw):
        super().__init__(**kw)

        self.ids['lv_categories'].adapter.bind(on_selection_change=self.lv_categories_on_selection_change)

    def open(self):
        self.ids['lv_categories'].adapter.data = []

        db = database()

        categories_data = db.get_categories()

        if categories_data:
            self.ids['lv_categories'].adapter.data = categories_data

        else:
            print("Error !!!")

    @staticmethod
    def lv_categories_args_converter(row, obj):
        return {'text': obj["category_name"], 'size_hint_y': None, 'height': 30}

    def lv_categories_on_selection_change(self, obj):
        if obj.selection:
            index = obj.selection[0].index
            selected = obj.data[index]

            self.parent.get_screen('notes').category_id = selected['id']
            self.parent.get_screen('notes').open()
            self.parent.current = 'notes'
