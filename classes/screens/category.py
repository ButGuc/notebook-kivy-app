from classes.data.database import database
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup


class delete_popup(Popup):
    category_id = None

    def yes(self, self_app):
        db = database()

        category_data = db.del_category(self.category_id)

        if category_data:
            self_app.root.get_screen('categories').open()
            self_app.root.current = 'categories'

        else:
            print("Delete Error")

        self.dismiss()


class category(Screen):

    task = None
    category_id = None
    previous_page = 'category'

    def open(self):
        if self.task == 'add':
            self.ids['header'].content = "Category Add"
            self.ids['ti_category_name'].text = ""

        elif self.task == 'edit':
            self.ids['header'].content = "Category Edit"

            db = database()

            category_data = db.get_category(self.category_id)

            self.ids['ti_category_name'].text = category_data["category_name"]

    def b_save_on_press(self, category_name):
        if self.task == 'add':
            if category_name == "":
                return

            db = database()

            category_data = db.add_category(category_name)

            if category_data:
                self.parent.get_screen(self.previous_page).open()
                self.parent.current = self.previous_page

        elif self.task == 'edit':
            if category_name == "":
                return

            db = database()

            category_data = db.set_category(category_name, self.category_id)

            if category_data:
                self.parent.get_screen(self.previous_page).open()
                self.parent.current = self.previous_page

    def b_delete_on_press(self):
        if self.task == 'add':
            pass

        elif self.task == 'edit':
            pop = delete_popup()
            pop.category_id = self.category_id
            pop.open()
