from classes.data.database import database
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup


class delete_popup(Popup):
    note_id = None

    def yes(self, self_app):
        db = database()

        category_data = db.del_note(self.note_id)

        if category_data:
            self_app.root.get_screen('notes').open()
            self_app.root.current = 'notes'

        self.dismiss()


class note(Screen):
    task = None
    note_id = None
    category_id = None

    def __init__(self, **kw):
        super().__init__(**kw)

    def open(self):
        if self.task == 'add':
            self.ids['header'].content = "Note Add"
            self.ids['ti_note_title'].text = ""
            self.ids['ti_note_content'].text = ""

        elif self.task == 'edit':
            db = database()

            note_data = db.get_note(self.note_id)

            self.ids['ti_note_title'].text = note_data["note_title"]
            self.ids['ti_note_content'].text = note_data["note_content"]

        else:
            pass

    def b_save_on_press(self, note_title, note_content):
        if self.task == 'add':
            if note_title == "" or note_content == "":
                return

            db = database()

            note_data = db.add_note(note_title, note_content, self.category_id)

            if note_data:
                self.parent.get_screen('notes').open()
                self.parent.current = 'notes'

        elif self.task == 'edit':
            if note_title == "" or note_content == "":
                return

            db = database()

            note_data = db.set_note(note_title, note_content, self.note_id)

            if note_data:
                self.parent.get_screen('notes').open()
                self.parent.current = 'notes'

    def b_delete_on_press(self):
        if self.task == 'add':
            pass

        elif self.task == 'edit':
            pop = delete_popup()
            pop.note_id = self.note_id
            pop.open()
