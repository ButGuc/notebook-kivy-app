from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from classes.data.auth import auth


class register_error_popup(Popup):
    pass


class register(Screen):

    def register(self, username, email, password, confirm_password):
        au = auth()

        if au.register(username, email, password, confirm_password):
            self.parent.get_screen('categories').open()
            self.parent.current = 'categories'

        else:
            popup = register_error_popup()
            popup.open()
