from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from classes.data.auth import auth


class login_error_popup(Popup):
    pass


class login(Screen):

    def login(self, username, password):

        au = auth()

        if au.login(username, password):
            self.parent.get_screen('categories').open()
            self.parent.current = 'categories'

        else:
            popup = login_error_popup()
            popup.open()
