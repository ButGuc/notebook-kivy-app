from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from classes.data.auth import database
from classes.data.auth import auth


class settings_error_popup(Popup):
    pass


class settings_info_popup(Popup):
    pass


class settings(Screen):
    previous_page = ''

    def open(self):
        au = auth()

        user_data = au.get()

        self.ids['ti_username'].text = user_data['username']
        self.ids['ti_email'].text = user_data['email']

    def update(self, username, email, password, confirm_password):
        au = auth()
        auth_data = au.get()

        if password != "":
            if password == confirm_password and username != "" and email != "":
                db = database()

                db.set_user_password(username, email, password, auth_data['token'])

                popup = settings_info_popup()
                popup.open()

            else:
                popup = settings_error_popup()
                popup.open()

        else:
            if username != "" and email != "":
                db = database()

                db.set_user(username, email, auth_data['token'])

                popup = settings_info_popup()
                popup.open()

            else:
                popup = settings_error_popup()
                popup.open()

    def logout(self):
        au = auth()
        au.logout()

        self.parent.current = 'login'
