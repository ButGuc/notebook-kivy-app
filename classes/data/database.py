import os
import json
import uuid
import sqlite3 as lite


class database:

    db = r'db.sqlite'

    def __init__(self):
        if os.path.isfile(self.db):
            try:
                self.connection = lite.connect(self.db)
                self.connection.row_factory = lite.Row
                self.cursor = self.connection.cursor()

            except lite.Error:
                print("Connection Error")

        else:
            self.connection = lite.connect(self.db)
            self.connection.row_factory = lite.Row
            self.cursor = self.connection.cursor()

            with self.connection:
                self.cursor.execute("create table categories (id integer primary key autoincrement, category_name text, user_id int)")
                self.cursor.execute("create table notes (id integer primary key autoincrement, note_title text, note_content text, category_id integer)")
                self.cursor.execute("create table users (id integer primary key autoincrement, username text, email text,password text, token text)")

    def user_id(self):
        auth_file = r'auth.json'

        with open(auth_file) as file:
            auth_data = json.load(file)

        user_data = self.get_user_with_token(auth_data['token'])

        if user_data:
            return user_data['id']

        else:
            return False

    def get_categories(self):
        try:
            with self.connection:
                self.cursor.execute("select * from categories where user_id == ?", [self.user_id()])
                rows = self.cursor.fetchall()
                rows_length = len(rows)

            if rows_length > 0:
                return rows

            else:
                return False

        except lite.Error:
            return False

    def add_category(self, category_name):
        try:
            with self.connection:
                self.cursor.execute("insert into categories (category_name, user_id) values(?, ?)", [category_name, self.user_id()])

            return True

        except lite.Error:
            return False

    def get_category(self, category_id):
        try:
            with self.connection:
                self.cursor.execute("select * from categories where id == ?", [category_id])
                row = self.cursor.fetchone()

            if row is not None:
                return row

            else:
                return False

        except lite.Error:
            return False

    def set_category(self, category_name, category_id):
        try:
            with self.connection:
                self.cursor.execute("update categories set category_name = ? where id == ?", [category_name, category_id])

            return True

        except lite.Error:
            return False

    def del_category(self, category_id):
        try:
            with self.connection:
                self.cursor.execute("delete from notes where category_id == ?", [category_id])
                self.cursor.execute("delete from categories where id == ?", [category_id])

            return True

        except lite.Error:
            return False

    def get_notes(self, category_id):
        try:
            with self.connection:
                self.cursor.execute("select * from notes where category_id == ?", [category_id])
                rows = self.cursor.fetchall()
                rows_length = len(rows)

            if rows_length > 0:
                return rows

            else:
                return False

        except lite.Error:
            return False

    def get_note(self, note_id):
        try:
            with self.connection:
                self.cursor.execute("select * from notes where id == ?", [note_id])
                row = self.cursor.fetchone()

            if row is not None:
                return row

            else:
                return False

        except lite.Error:
            return False

    def add_note(self, note_title, note_content, category_id):
        try:
            with self.connection:
                self.cursor.execute("insert into notes (note_title, note_content, category_id) values(?, ?, ?)", [note_title, note_content, category_id])

            return True

        except lite.Error:
            return False

    def set_note(self, note_title, note_content, note_id):
        try:
            with self.connection:
                self.cursor.execute("update notes set note_title = ?, note_content = ? where id == ?", [note_title, note_content, note_id])

            return True

        except lite.Error:
            return False

    def del_note(self, note_id):
        try:
            with self.connection:
                self.cursor.execute("delete from notes where id == ?", [note_id])

            return True

        except lite.Error:
            return False

    def get_user_return_token(self, username, password):
        try:
            with self.connection:
                self.cursor.execute("select * from users where username == ? and password == ?", [username, password])
                row = self.cursor.fetchone()

            if row is not None:
                token = str(uuid.uuid4())

                with self.connection:
                    self.cursor.execute("update users set token = ? where username == ? and password == ?", [token, username, password])

                return token

            else:
                return False

        except lite.Error:
            return False

    def get_user_with_token(self, token):
        try:
            with self.connection:
                self.cursor.execute("select * from users where token == ?", [token])
                row = self.cursor.fetchone()

            if row is not None:
                return row

            else:
                return False

        except lite.Error:
            return False

    def get_user_with_email(self, email):
        try:
            with self.connection:
                self.cursor.execute("select * from users where email == ?", [email])
                row = self.cursor.fetchone()

            if row is not None:
                return True

            else:
                return False

        except lite.Error:
            return False

    def add_user(self, username, email, password):
        try:
            token = str(uuid.uuid4())

            with self.connection:
                self.cursor.execute("insert into users (username, email, password, token) values(?, ?, ?, ?)", [username, email, password, token])

            return token

        except lite.Error:
            return False

    def set_user(self, username, email, token):
        try:
            with self.connection:
                self.cursor.execute("update users set username = ?, email = ? where token == ?", [username, email, token])

            return True

        except lite.Error:
            return False

    def set_user_password(self, username, email, password, token):
        try:
            with self.connection:
                self.cursor.execute("update users set username = ?, email = ?, password = ? where token == ?", [username, email, password, token])

            return True

        except lite.Error:
            return False

    def del_user_token(self, token):
        try:
            with self.connection:
                self.cursor.execute("update users set token = '' where token == ?", [token])

            return True

        except lite.Error:
            return False

    def del_user(self, user_id):
        try:
            with self.connection:
                self.cursor.execute("delete from users where id == ?", [user_id])

            return True

        except lite.Error:
            return False
