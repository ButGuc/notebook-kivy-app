import os
import json
from classes.data.database import database


class auth:
    auth_file = r'auth.json'

    def __init__(self):
        if not os.path.isfile(self.auth_file):
            auth_data = {'username': '', 'token': ''}

            with open(self.auth_file, 'w') as outfile:
                json.dump(auth_data, outfile)

    def get(self):
        with open(self.auth_file) as file:
            auth_data = json.load(file)

        db = database()

        user_data = db.get_user_with_token(auth_data['token'])

        if user_data:
            return user_data

        else:
            return False

    def check(self):
        db = database()

        with open(self.auth_file) as file:
            auth_data = json.load(file)

        if auth_data['token'] is not '':
            user_data = db.get_user_with_token(auth_data['token'])

            if user_data:
                return True

            else:
                return False

    def login(self, username, password):
        db = database()

        user_data = db.get_user_return_token(username, password)

        if user_data:
            auth_data = {'username': username, 'token': user_data}

            with open(self.auth_file, 'w') as outfile:
                json.dump(auth_data, outfile)

            return True

        else:
            return False

    def logout(self):
        db = database()

        with open(self.auth_file) as file:
            auth_data = json.load(file)

        user_data = db.del_user_token(auth_data['token'])

        if user_data:
            auth_data = {'username': '', 'token': ''}

            with open(self.auth_file, 'w') as outfile:
                json.dump(auth_data, outfile)

            return True

        else:
            return False

    def register(self, username, email, password, confirm_password):
        db = database()

        if password != confirm_password:
            return False

        if db.get_user_with_email(email):
            return False

        user_data = db.add_user(username, email, password)

        print(user_data)

        if user_data:
            auth_data = {'username': username, 'token': user_data}

            with open(self.auth_file, 'w') as outfile:
                json.dump(auth_data, outfile)

            return True

        else:
            return False
