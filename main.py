from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager
from kivy.core.window import Window
from classes.screens.login import login
from classes.screens.register import register
from classes.screens.settings import settings
from classes.screens.categories import categories
from classes.screens.category import category
from classes.screens.notes import notes
from classes.screens.note import note
from classes.data.database import database
from classes.data.auth import auth


class notebook(App):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.sm = ScreenManager()

        Window.size = (400, 500)

        database()

    def build(self):
        # Screens
        Builder.load_file('interface/screens/login.kv')
        Builder.load_file('interface/screens/register.kv')
        Builder.load_file('interface/screens/settings.kv')
        Builder.load_file('interface/screens/categories.kv')
        Builder.load_file('interface/screens/category.kv')
        Builder.load_file('interface/screens/notes.kv')
        Builder.load_file('interface/screens/note.kv')

        # Widgets
        Builder.load_file('interface/widgets/header.kv')

        screens = [
            login(name='login'),
            register(name='register'),
            settings(name='settings'),
            categories(name='categories'),
            category(name='category'),
            notes(name='notes'),
            note(name='note'),
        ]

        for item in screens:
            self.sm.add_widget(item)

        auth_obj = auth()

        if auth_obj.check():
            self.sm.get_screen('categories').open()
            self.sm.current = 'categories'

        else:
            self.sm.current = 'login'

        return self.sm


if __name__ == '__main__':
    notebook().run()
